---
title: "What I'm Working on this Year"
date: 2020-07-08T22:21:33-04:00
draft: true
---

# New Year
This year saw a great change for my career. Since I am currently a Master's student at Georgia Tech, I qualified to apply for a Pathways engineer position at Kennedy Space Center. To my surprise, I was offered a position late last year and started orientation in January.

In this new adventure, I have been the software architect / lead developer / technical writer for a new request tracking system needed by one of the metrology teams on center. (In case you couldn't tell, I am wearing many hats for this project). Docker has been a huge time saver in this area too. The project involves a database, rest api, and front end so splitting up the application into containers has saved my bacon more than once specifically in cases where I need to rebuild the database while leaving the rest api running.

What I really want to talk about is education because I still have a ton of learning to do.

Last year I realized I wanted to be a roboticist. This year I'm learning what that word means.

# Still Learning

In the Spring at Georgia Tech (*studying online*), I took [Software Analysis]() and [Cyber Physical Systems and Design]() which were very rigorous and interesting courses. In the former, I learned about static and dynamic analysis techniques for programs, used llvm, discovered prolog, and ultimately found that a good program is more than just one that has unit tests.

In the latter, 
