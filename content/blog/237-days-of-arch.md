---
title: "237 Days of Arch"
date: 2020-04-30T22:09:30-04:00
draft: true
---

![terminal](/static/img/days-of-arch/terminal.png)

> Any piece of information from this screenshot helps explain what made my experience so awesome.



Three years ago, you could not convince me to run Linux to save my life. At the time, I was convinced that there was no benefit to me learning the "complexities" involved in leaving Windows to go to something else. Well eventually, one of my best friends got through to me and convinced me to try it. His argument that won me over was "you should at least learn Linux now in-case you need it for a job later." I started with Ubuntu 18.04 and working with Vim as a text editor. I wiped my Windows partition on my only laptop for good measure and I dove headfirst into figuring out how the distro worked and how I could customize it. This later led me to KDE and a JavaScript-based Tiling windows manager. My laptop was looking beautiful and perfect but there was an itch I had to scratch: Arch Linux. 

A full year into Ubuntu and I knew there was more I could learn. In the words of David Tennant:

![terminal](/static/img/days-of-arch/doctorquien.gif)

Thus in the Fall of 2019, I went down the path of replacing my setup with Arch Linux. I struggled hard. I could not get the install to work on my laptop from various instructions online. I was starting to think *I* was the problem (and I definitely was) but then I found the ArchLabs guided installer which helped me finish the task. Was it cheating? Probably. But I ended up LightDM and i3. Mission accomplished!

After this, my learning really took off especially as I was forced to learn how to work in an environment with no pre-installed GUI tools to help me do functions like connect to a different monitor. 

It has been a crazy fun ride and I have a beautiful setup. But I am going back to Debian after 237 days of Arch.

## Things I Loved about Arch

### i3

![whole screen](/static/img/days-of-arch/screen.png)

## Why I'm Leaving

## Returning is Inevitable

