---
title: What I learned working with NASA on a Robotics Project
description: 'Lesson Learned: Work on Projects you are Passionate About'
date: '2019-10-11T19:38:15.570Z'
categories: []
keywords: []
slug: >-
  /Lessons-Learned
---

### Lesson Learned: Work on Projects you are Passionate About

![_The first thing I ever saw about the EZ-RASSOR… a PowerPoint presentation_](/static/img/1__3JZtVSH3c__MqRdamLPj__Sw.png)
_The first thing I ever saw about the EZ-RASSOR… a PowerPoint presentation_

Before getting assigned to this project, I found myself sitting in the first lecture for my capstone Senior Design class at the University of Central Florida (UCF). In the first semester for this class, all of the students were introduced to various projects that needed developers. These projects ranged from StarCraft II machine learning projects to interdisciplinary projects involving the creation of a new autonomous drone. However, as the days went on in the discovery phase, I found that I was not really interested in any particular project. That is, until a new project was introduced from the [Florida Space Institute](https://fsi.ucf.edu/). This was the **NASA** **EZ-RASSOR** project. It was pitched as an educational project, where the assigned developers would be responsible for building all of the software on a brand-new NASA excavation robot. Engineers at NASA Swamp Works would design the hardware for a 3D-print-based robot and UCF Developers would design the software to operate it. Better yet was that any code produced would be public and open source to better fit the vision of this project ending up in the hands of students.

Almost immediately after hearing all of this, I knew it was the one for me and I dedicated as much time as possible into writing a strong cover letter and polishing up my resume so that I would get selected for the project. After weeks of waiting, I was beyond delighted to find out that I was selected for the project.

The passion I had made me a more successful developer which is an important lesson to any developer working on a major open source project: work on something you are passionate about.

In some ways, I imagine I was having the same experience as developers working on stand-ups; my life ended up revolving around the project. If I wasn’t developing for the project in my spare time, I was researching a design challenge we would encounter. Twice over this project, I ended up pulling all-nighter sessions with the team since I was stuck in code while time just passed me by. Now, my point here isn’t that developers should just devote every iota of their life to a project until completion, but just that having passion helps you **_to want to tackle_** challenges that come up in a project. Without passion, doing the bare minimum is more likely to occur.

### Lesson Learned: Work with People who are also Passionate

![The majority of what we dubbed “The Squad” during a scrubbed Falcon 9 Launch](/static/img/1__tDSPf5__pvWhoiBq3GErs5w.jpeg)
The majority of what we dubbed “The Squad” during a scrubbed Falcon 9 Launch

When dealing with a project of this nature, no one achieves anything alone. That also means that it is important to have the right people on your team, especially to fill the knowledge gap for all of the skill sets needed for this project. Here is the full set of requirements that were given to us at the beginning of NASA EZ-RASSOR project:

*   Design a full software architecture to operate the EZ-RASSOR
*   Configure a full simulator to test the EZ-RASSOR
*   Develop manual and autonomous controls for the robot
*   Add and configure stereo cameras and motors for the robot

This list quickly grew as each of these requirements was further broken down into multiple branches and dependencies. Only 10 computer science majors were assigned to work on this project with myself as the project manager. I certainly did not have the experience or hands needed to achieve half of those requirements, none of us did _al_one. To handle this, the team into four sub-teams. This ended up creating a true reliance on each other: if any sub-team failed, we all failed.

Despite this, I know that our project only succeeded all 10 of us believed in what we were doing and had each other’s backs. We knew how to _work_ and _have fun_ which boosted productivity and allowed us to trust each other enough to ask for help when it was needed.

![The Swamp Works lab we worked out of every Friday (credit [nasa.gov](https://nasa.gov))](/static/img/1__woAAEWbWTeyk2dXWAHQfxQ.jpeg)
The Swamp Works lab we worked out of every Friday (credit [nasa.gov](https://nasa.gov))

### Lesson Learned: Use ROS — It’s Extremely Powerful and Fun to Use

First off, ROS is the most important part of this project. This key piece was an architecture choice that remained constant throughout the project and was an obvious choice to accomplish our requirements. Therefore it would be important to explain what ROS is. ROS stands for the “Robot Operating System”. However, it’s not _that_ kind of operating system in the way we often think of with a GUI. Instead, I like to think of it as a “System” to “Operate” “Robots”. In a nutshell, it is middle-ware to allow users to develop and operate a robot.

As a messaging platform, it uses a publisher/subscriber system to transport messages between topics which are then passed down to a specific process (node). These nodes are the actual functions that the system performs like applying a particular force to a joint, rotating the front wheels 15 degrees to the right, and even starting up a simulation. From this inter-connectivity, we can make the EZ-RASSOR and its functions very modular. To demonstrate this, note the ROS graph shown below from the project (note: the squares represent topics and the circles represent nodes):

![The final version of our ROS graph with pretty colors](/static/img/1__ikLFM__7LMCGjdPXWuxdJ9g.jpeg)
The final version of our ROS graph with pretty colors

Hopefully the color coordination helps show the flow of control in our system. This modularity was the best tool our team had since we could focus on our individually assigned tasks without having to worry about the efforts of others. For example, when I worked on new ROS nodes to handle input from our mobile application, all I needed to know what the topic that I needed to publish the interpreted values to. Also, depending on the message itself, our topics could know which nodes to call upon which came in handy when we wanted to operate either just the simulation or just the hardware.

This, to me, is just the basic explanation of ROS. As a framework it provides even more functionality to allow the robot to function. Through the use of ROS _.launch_ files, the EZ-RASSOR software could start by first launching the simulation (which we did through Gazebo) and waiting for controller commands. Additionally we could use the .launch files to skip the simulation entirely and instead have the actual _hardware_ wait for controller commands. Lastly, what I think is great about ROS is that we could have the system do both. We could demonstrate the hardware moving in the real world while showing a real-time simulation of the EZ-RASSOR on the Moon for emphasis.

Working with this system has given me a brand new understanding of robotics. Learning this platform was an experience where I would attempt to learn one subject, like how to publish a message using Python, and end up doing a deep dive on how ROS even implements topics behind the scenes. While the documentation for ROS is mostly comprehensive, I actually ended up learning more from forums in the ROS community where one user would ask a question and other users would collaborate on answering that question. These constant-displays-of-teamwork encouraged me in this project to produce the highest quality code possible for the sake of helping others.

### Lesson Learned: Build a Roadmap but Expect it to Grow Over Time

![A task chart that was often hard to follow](/static/img/1__2e6I3XjXr__Sy6nYG9__AfaA.jpeg)
A task chart that was often hard to follow

My team of 10 developers initially had a large amount of ideas for the project and the direction(s) in which we wanted to take the project. Better yet, our project sponsor was very generous in allowing us to define the requirements ourselves as long as we achieved the main goals of the EZ-RASSOR project. This freedom was exciting and the NASA engineers even gave us the option to decide how we would accomplish the main software goals we had. Time to go to the Moon with our newfound autonomy!

As you can probably imagine, this amount of freedom became very limiting very quickly. Even though I had established leadership positions for different sub-teams in this project, the non-leads on the team would come up with ideas that were often even better than what the leads proposed. Stand-ups turned to deep dives into a single person’s last minute feature idea. I knew something had to change if we were ever going to get this project o the ground. Well, just before losing all of my hair, I decided that we needed a Statement of Work document with a defined list of software requirements and milestones that we would commit to completing before the project deadline. Drafting this document with the team caused a change in pace; no longer were we having side-bar conversations about stretch goals but we were all forced to think about what we could truly commit to accomplishing in our 8 month time frame. Finally after two weeks, we had a final Statement of Work document that was signed off from the project stakeholders.

Through this I learned that it is important to have a vision of what needs to be accomplished in a project, especially a written one that can be shared. We found it best to have our requirements explicitly list features instead of architecture. Committing to providing features gave us the opportunity to discuss the architecture during our weekly stand-ups and make any changes when needed while still ensuring that we stayed “on target” with our features.

### Lesson Learned: Don’t Underestimate the importance of the Elevator Pitch

![The final project presentation to UCF Faculty, our FSI Sponsor (Mike Conroy), and our NASA Software Engineer SME (Kurt Leucht)](/static/img/1__xQWU6i6gmvVvPczjP__VHkw.jpeg)
The final project presentation to UCF Faculty, our FSI Sponsor (Mike Conroy), and our NASA Software Engineer SME (Kurt Leucht)

I have gained a plethora of knowledge from watching Ted talks and attending developer conferences. Part of this knowledge is knowing how to explain a concept and still keep the audience engaged with things like relevant jokes. Truthfully, this project increased my “conference speaking” skills as I was called upon multiple times to present this project in lecture halls and even at the Swamp Works conference room in Kennedy Space center (twice!). It almost came naturally to me; giving a passionate talk about this project was easy because of the time investment I had made in it.

> And by easy, I mean that it was only easy to explain to people who already knew about the project.

I quickly found that I had a hard time explaining our project to people who knew nothing about it or the RASSOR. This was because I wasn’t prepared with an essential tool: the elevator pitch. Before having a pitch, the only way I knew to best explain our project involved a slide deck or an hours worth of time. I would try to explain how awesome the technological aspects of the project were but often ended up sounding garrulous. So how did I develop my elevator pitch?

First, I realized my elevator pitch would have to be short. Like short enough to say in one really big breath (about 2–3 sentences). Obviously, this required a lot of practice to get just right. But keeping it short and sweet allowed the listener to ask follow-up questions and help guide the conversation based on _their_ interests, instead of me giving them a 30 minute monologue on what _I_ _alone_ thought was cool about the project. Second, to prepare for the conversation that will ensue after the pitch, know your audience: i.e., technical vs. non-technical individuals. One group may capture more from me explaining why ROS was critical to our success while another group may only want to focus on the space-aspect of our project.

### Final Lesson Learned: Learn how to Learn

This is certainly an overstated point but being in Computer Science means that you are constantly learning. However, I discovered that bias and comfort led me to only research things that were familiar to me.

Before starting this project, I was a diehard Microsoft fan (I mean, my handle isn’t [@CSharpRon](https://twitter.com/CSharpRon) for nothing.) Shortly after starting this project, we noticed that Ubuntu would be the best platform to use to get up and running with our project. Knowledge of Bash would also be critical. Oh, and my teammates kept recommending this weird voodoo magic text-editor called Vim. I knew a whole lot of information on Windows-based development tools as well as the inner workings of the OS, but was surprised when I realized I would need to learn something completely different than what I was used to.

Did I revolt? Of course not, I put my comforts aside and went all-in on learning Linux and setting up my development environment. I went so far that I deleted my Windows OS and replaced it with Ubuntu. (Thank goodness I did too, because my Dell XPS 15 laptop was the only one in the group that could consistently handle the simulation running at the same time as our AI tools.)

![Our EZ-RASSOR on the Moon (if only it were around during Apollo)](/static/img/1____YD4h5M6dOeNsYX9yc5yRA.gif)
Our EZ-RASSOR on the Moon (if only it were around during Apollo)

> Now I run Arch Linux as my daily driver and am a much stronger developer than I was a year ago.

So, if there are any takeaways from this post it would be to be passionate about what you do and never become stubborn to the point where you refuse to learn something new. These lessons have carried over into my current job at the Kennedy Space Center where I am playing my part to help ensure the success of the Artemis mission. I am and will be forever grateful for the experiences the 10 of us had and the lessons I learned working with NASA on a robotics project.

Want a technical discussion on how the EZ-RASSOR functions? Check out our [introductory blog post](https://medium.com/ez-rassor/introducing-the-ez-rassor-35dd0eb5c121) or our [GitHub repository](https://github.com/FlaSpaceInst/EZ-RASSOR) for more information.
