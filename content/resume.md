---
title: "Resume"
date: 2020-06-30T10:42:24-04:00
draft: false
---
https://CSharpRon.dev
[GitHub](https://github.com/CSharpRon) | [Medium](https://medium.com/@CSharpRon) | [Twitter](https://twitter.com/CSharpRon)

---

## Education 
> {{< figure position="left" src="/static/georgia-tech-svg.png" title="Georgia Tech" style="height:150px;width:140" >}}
> ### Master's of Science in Computer Science 
> *(Specialization in Computational Perception & Robotics)* 
> + Georgia Tech
> + In progress
> + Expected Graduation Date: May 2022
>  
> ### Bachelor's of Science in Computer Science    
> 
> + University of Central Florida   
> + Graduation Date: May 2019   
>     
> ### Certified Scrum Master   
>     
> + Certified: March 2018   


## Job Experience 
> {{< figure position="left" src="/static/nasa-logo-svg.png" title="NASA Logo" style="height:200px;width:140;" >}}
> ### Software Engineer: Pathways - NASA
> + January 2020 - Present
> + Description: In this role, I am developing software systems for metrology work at the Kennedy Space Center. The technologies for this system include PostgresQL, Docker, Go, and React.
> 
> ### Software Engineer II - ERC (TOSC Contract at KSC)
> + August 2019 - January 2020 
> + Description: Support the Software Development Lifecycle related to the ground systems at Kennedy Space Center for the Artemis missions. This work includes creating software for the Launch Control Center as well as working with ground engineers to create software needed for their monitoring and operations.
>
> ### Software Developer - Orlando Health 
> + January 2018 - August 2019
> + Description: Design and implement new line-of-business applications for Orlando Health, as well as code bug fixes and enhancements to existing projects. 
>
> ### Software Developer Intern - NASA
> + October 2018 - May 2019
> + Description: Design a simulation in Gazebo for an internal rover project based on NASA's [RASSOR project](https://technology.nasa.gov/patent/KSC-TOPS-7), complete project documentation, and operate as Project Manager over the 10-person internship team.
> 
> ### IT Tech - Orlando Health
> 
> + July 2015 - January 2018
> + Description: Troubleshoot and maintain organization computers and systems, work directly with physicians, nurses, etc. to resolve a wide range of technical issues, serve as an escalation point to other Field Support team members, and manage SharePoint 2010 department site and subsites.

## Robotics Projects
> {{< figure position="left" src="/static/ezrassor.jpeg" title="EZRASSOR" style="height:200px;width:140;" >}}
> ### EZ-RASSOR (Easy Regolith Advanced Surface System Operations Robot)
> + September 2018 - Present   
> + Description: This project is a software suite based on ROS to control a small excavation robot that is being created by the Florida Space Institute and NASA for education. 
> + Technologies Used: ROS Kinetic, ROS Melodic, Gazebo 7, Gazebo 9, Python 2, React Native, C++11, Qt     
> + Roles: ROS/Simulation Developer and Team Lead

## Technical Skills
<center>   

Technologies        |Languages      |Frameworks/Tools               |Operating Systems
--------------------|:-------------:|-------------------------------|---------------|
ROS Kinetic/Melodic |Python         |Entity Framework               |Ubuntu 16.04/18.10
MVC                 |C/C++          |ASP.NET                        |Windows 7, 10
Blender             |T-SQL          |Microsoft SQL Server           |
Unity               |HTML           |Oracle SQL Server              |
Docker              |JavaScript     |Xamarin|
Jenkins             |ColdFusion     ||
Azure Management/AD |React/Node.JS  ||
Vim                 |Java           ||
Redis Cache         |C#             ||
Qt Creator          |Bash           ||
_                   |Go             ||
|||  

</center> 

