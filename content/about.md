---
title: "About"
date: 2019-11-13T11:59:40-05:00
---
![Profile Picture](/static/profile.jpg)   
I am currently a Software Engineer Pathways Intern working on software systems for NASA's [Artemis Program](https://www.nasa.gov/specials/artemis/). Space is one of my favorite things to talk about and one of the areas where I am glad to put my skills to use. 

I am also a Master's student at Georgia Tech pusuing a Computer Science degree with a focus on Robotics. Thanks to their online program [OMSCS](https://www.omscs.gatech.edu/) I am able to somehow find time to do work-work and school-work. This brings be to my next highlight: Robotics!

Robotics to me is one of those fields that is only growing and has a long way to go before reaching critical mass. I and 9 others got to create open-source software for an educational robot called the [EZ-RASSOR](https://github.com/FlaSpaceInst/EZ-RASSOR) that we still maintain and work with others to improve.

If you want to talk about any of my interests or need a collaborator for a project, I'd love to help.

Teamwork makes the dream work.

So does Linux.

---
## To get in touch with me
> ronald.marrero@outlook.com   
> [https://linkedin.com/in/csharpron](https://linkedin.com/in/csharpron)  
> [https://twitter.com/csharpron](https//twitter.com/csharpron)