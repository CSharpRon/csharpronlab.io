---
title: "Secret Gift"
date: 2019-12-25T11:02:39-05:00
draft: false
--- 

![Beach-Blur](/static/img/beach-blur.jpg)

## Hi Ashley!

So, I see you have found your way to this QR Code. "What's Ron up to?" I bet you're asking yourself...

Well, I thought I would tell you where we are going for our honeymoon a whole 10 months early! Except, I seem to have been hit with amnesia and don't remember the name of the destination. All I have is this picture.

If you can guess where this photo was taken, then you will have discovered the destination :) Otherwise, we will both randomly hop on a plane come October 17th.

Also, as cool as it would be, it's not **this** beach:

![Proposal](/static/img/proposal.jpg)
but it is a beach with plenty of sol

While you're guessing away, [check out this link](https://cdn.shopify.com/s/files/1/1906/8777/files/TRB-Gift-Printout-HR.pdf?3539417630129812618) for a gift that is headed your way as we speak. You should expect it between January 4 - 8.

I love you now and always. You will forever be my greatest adventure.

Love,

Ron
