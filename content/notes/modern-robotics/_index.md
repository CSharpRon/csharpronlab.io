---
cascade:
    banner: /images/modern-robotics-banner.jpg
title: "Modern Robotics"
date: 2020-07-11T10:25:01-04:00
layout: list
draft: false
---

This is a collection of courses from Coursera's [Modern Robotics: Mechanics, Planning, and Control specialization](https://www.coursera.org/specializations/modernrobotics). <br/>Thank goodness for LaTex support!  
> Written and exported to HTML via [MarkText](https://github.com/marktext/marktext)

