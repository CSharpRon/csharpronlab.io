---
cascade:
    banner: /images/notes.png
title: "Notes"
date: 2020-07-11T10:25:01-04:00
draft: false
---

A collection of personal development notes for code-related courses, conferences I attend etc.

## Usage
All of the notes are written with Markdown but may have LaTeX code embedded. 
For the best experience in reading the docs, use a markdown editor that has support for LaTeX like [MarkText](https://marktext.app/) or [Zettlr](https://www.zettlr.com/)


---