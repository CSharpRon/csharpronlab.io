# CSharpRon GitHub Pages Site

![](https://gitlab.com/csharpron/csharpron.dev/badges/main/pipeline.svg)  
This is the code for my personal website hosted through Gitlab pages.
![Website](static/website.jpg)

# Installation
To run this website locally, first [install Hugo.](https://gohugo.io/getting-started/installing/)

Go into the folder where you cloned this repository.

``` sh
git submodule update --init --recursive # Needed for custom theme
```

# Usage
```sh
hugo server -D # The application will then be running via an exposed port on localhost
```

# Deployment
Through the simple Gitlab CI/CD pipeline, commits to this repo will trigger a hugo build and a website deployment.